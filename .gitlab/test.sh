#!/usr/bin/env bash
###
#
#  @file test.sh
#  @copyright 2023-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
#                       Univ. Bordeaux. All rights reserved.
#
#  @version 1.0.0
#  @author Mathieu Faverge
#  @author Abel Calluaud
#  @date 2023-12-14
#
###

set -e
set -x

if [[ "$SYSTEM" == "windows" ]]; then
    # this is required with BUILD_SHARED_LIBS=ON
    export PATH="/c/Windows/WinSxS/x86_microsoft-windows-m..namespace-downlevel_31bf3856ad364e35_10.0.19041.1_none_21374cb0681a6320":$PATH
    export PATH=$PWD/build/src:$PWD/build/tests:$PWD/build/wrappers/fortran90:$PATH
fi

ctest --test-dir build --output-junit ../${LOGNAME}-junit.xml --output-on-failure --no-compress-output $TESTS_RESTRICTION

if [[ "$SYSTEM" == "linux" ]]; then
    # clang is used on macosx and it is not compatible with MORSE_ENABLE_COVERAGE=ON
    # so that we can only make the coverage report on the linux runner with gcc
    lcov --capture --directory build --output-file ${LOGNAME}.lcov
fi
