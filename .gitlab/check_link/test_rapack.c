/**
 *
 * @file test_rapack.c
 *
 * Tests and validate linking with the Rapack library.
 *
 * @copyright 2015-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Abel Calluaud
 * @date 2023-12-14
 *
 * @precisions normal z -> c d s
 *
 **/
#include <rapack.h>

int main( int argc, char *argv[] ) {

    rpk_matrix_t A;
    rpk_dlralloc( 10, 10, 10, &A );
    rpk_dlrfree( &A );

    (void)argc;
    (void)argv;
    return EXIT_SUCCESS;
}
