/**
 *
 * @file rpk_zge2lr_svd.c
 *
 * RAPACK low-rank kernel routines using SVD based on Lapack ZGESVD.
 *
 * @copyright 2016-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Gregoire Pichon
 * @author Esragul Korkmaz
 * @author Mathieu Faverge
 * @author Nolan Bredel
 * @date 2024-01-08
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "frobeniusupdate.h"
#include "rpk_z.h"
#include "rpk_znancheck.h"

/**
 *******************************************************************************
 *
 * @brief Convert a full rank matrix in a low rank matrix, using SVD.
 *
 *******************************************************************************
 *
 * @param[in] use_reltol
 *          TODO
 *
 * @param[in] tol
 *          The tolerance used as a criterion to eliminate information from the
 *          full rank matrix.
 *          If tol < 0, then we compress up to rklimit. So if rklimit is set to
 *          min(m,n), and tol < 0., we get a full representation of the matrix
 *          under the form U * V^t.
 *
 * @param[in] rklimit
 *          The maximum rank to store the matrix in low-rank format. If
 *          -1, set to min(M, N) / RAPACK_LR_MINRATIO.
 *
 * @param[in] m
 *          Number of rows of the matrix A, and of the low rank matrix Alr.
 *
 * @param[in] n
 *          Number of columns of the matrix A, and of the low rank matrix Alr.
 *
 * @param[in] Avoid
 *          The matrix of dimension lda-by-n that needs to be compressed
 *
 * @param[in] lda
 *          The leading dimension of the matrix A. lda >= max(1, m)
 *
 * @param[out] Alr
 *          The low rank matrix structure that will store the low rank
 *          representation of A
 *
 *******************************************************************************
 *
 * @return  TODO
 *
 *******************************************************************************/
rpk_fixdbl_t
rpkx_zge2lr_svd( const rpk_ctx_t *ctx,
                 rpk_int_t        rklimit,
                 rpk_int_t        m,
                 rpk_int_t        n,
                 const void      *Avoid,
                 rpk_int_t        lda,
                 rpk_matrix_t    *Alr )
{
    const rpk_complex64_t *A     = (const rpk_complex64_t *)Avoid;
    rpk_fixdbl_t           flops = 0.0;
    rpk_complex64_t       *u, *v, *zwork, *Acpy, ws;
    double                *rwork, *s;
    rpk_int_t              i, ret, ldu, ldv;
    rpk_int_t              minMN, imax;
    rpk_int_t              lwork = -1;
    rpk_int_t              zsize, rsize;
    double                 norm;
    rpk_fixdbl_t           tol        = ctx->tolerance;
    int                    use_reltol = ctx->use_reltol;

#if !defined(NDEBUG)
    if ( m < 0 ) {
        return -2;
    }
    if ( n < 0 ) {
        return -3;
    }
    if ( lda < m ) {
        return -5;
    }
#endif

    norm = LAPACKE_zlange_work( LAPACK_COL_MAJOR, 'f', m, n,
                                A, lda, NULL );

    /* Quick return on norm */
    if ( ( norm == 0. ) && ( tol >= 0. ) ) {
        rpk_zlralloc( m, n, 0, Alr );
        return 0.;
    }

    rklimit = ( rklimit < 0 ) ? ctx->get_rklimit( ctx, m, n ) : rklimit;
    if ( tol < 0. ) {
        tol = -1.;
    }
    else if ( use_reltol ) {
        tol = tol * norm;
    }

    /* Quick return on max rank */
    minMN   = rpk_imin( m, n );
    rklimit = rpk_imin( minMN, rklimit );

    /**
     * If maximum rank is 0, then either the matrix norm is below the tolerance,
     * and we can return a null rank matrix, or it is not and we need to return
     * a full rank matrix.
     */
    if ( rklimit == 0 ) {
        if ( ( tol < 0. ) || ( norm < tol ) ) {
            rpk_zlralloc( m, n, 0, Alr );
            return 0.;
        }

        /* Return full rank */
        rpk_zlralloc( m, n, -1, Alr );
        ret = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'A', m, n,
                                   A, lda, Alr->u, Alr->rkmax );
        assert( ret == 0 );
        return 0.;
    }

    /*
     * Allocate a temporary Low rank matrix to store the full U and V
     */
    rpk_zlralloc( m, n, rpk_imin( m, n ), Alr );
    u   = Alr->u;
    v   = Alr->v;
    ldu = m;
    ldv = Alr->rkmax;

    /*
     * Query the workspace needed for the gesvd
     */
#if defined( RAPACK_DEBUG_LR_NANCHECK )
    ws = minMN;
#else
    {
        /*
         * rworkfix is a fix to an internal mkl bug see:
         * https://community.intel.com/t5/Intel-oneAPI-Math-Kernel-Library/MKL-2021-4-CoreDump-with-LAPACKE-cgesvd-work-or-LAPACKE-zgesvd/m-p/1341228
         */
        double rwork;
        ret = MYLAPACKE_zgesvd_work( LAPACK_COL_MAJOR, 'S', 'S',
                                     m, n, NULL, m,
                                     NULL, NULL, ldu, NULL, ldv,
                                     &ws, lwork, &rwork );
        (void)rwork;
    }
#endif
    lwork = ws;
    zsize = ws;
    zsize += m * n; /* Copy of the matrix A */

    rsize = minMN;
#if defined(PRECISION_z) || defined(PRECISION_c)
    rsize += 5 * minMN;
#endif

    zwork = malloc( zsize * sizeof(rpk_complex64_t) + rsize * sizeof(double) );
    rwork = (double*)(zwork + zsize);

    Acpy = zwork + lwork;
    s    = rwork;

    /*
     * Backup the original matrix before to overwrite it with the SVD
     */
    ret = LAPACKE_zlacpy_work(LAPACK_COL_MAJOR, 'A', m, n,
                              A, lda, Acpy, m );
    assert( ret == 0 );

    ret = MYLAPACKE_zgesvd_work( LAPACK_COL_MAJOR, 'S', 'S',
                                 m, n, Acpy, m,
                                 s, u, ldu, v, ldv,
                                 zwork, lwork, rwork + minMN );
    if ( ret != 0 ) {
        rapack_print_error( "SVD Failed\n" );
    }

    /* Let's stop i before going too far */
    imax = rpk_imin( minMN, rklimit + 1 );
    for ( i = 0; i < imax; i++, v += 1 ) {
        double frob_norm;

        /*
         * There are two different stopping criteria for SVD to decide the
         * compression rank:
         *	1) The 2-norm:
         *         Compare the singular values to the threshold
         *      2) The Frobenius norm:
         *         Compare the Frobenius norm of the trailing singular values to
         *         the threshold. Note that we use a reverse accumulation of the
         *         singular values to avoid accuracy issues.
         */
#if defined(RAPACK_SVD_2NORM)
        frob_norm = s[i];
#else
        frob_norm = rpk_dlassq( minMN - i, s + minMN - 1, -1 );
#endif

        if ( frob_norm < tol ) {
            break;
        }
        cblas_zdscal( n, s[i], v, ldv );
    }

    /*
     * Resize the space used by the low rank matrix
     */
    rpkx_zlrsze( ctx, 1, m, n, Alr, i, -1, rklimit );

    /*
     * It was not interesting to compress, so we restore the dense version in Alr
     */
    if ( Alr->rk == -1 ) {
        ret = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'A', m, n,
                                   A, lda, Alr->u, Alr->rkmax );
        assert( ret == 0 );
    }

    (void)ret;
    free( zwork );
    zwork = NULL;
    return flops;
}
