/**
 *
 * @file rpk_zlr2xx.c
 *
 * RAPACK low-rank kernel routines that perform the addition of AB into C.
 *
 * @copyright 2016-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Gregoire Pichon
 * @author Pierre Ramet
 * @author Abel Calluaud
 * @date 2024-03-11
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

#ifndef DOXYGEN_SHOULD_SKIP_THIS
static rpk_complex64_t zone  = 1.0;
static rpk_complex64_t zzero = 0.0;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

/**
 *******************************************************************************
 *
 * @brief Perform the addition of the low-rank matrix AB and the full-rank
 * matrix C.
 *
 *******************************************************************************
 *
 * @param[in] ctx
 *          The rapack context that defines the compression parameters to use.
 *
 * @param[inout] params
 *          The LRMM structure that stores all the parameters used in the LRMM
 *          functions family.
 *          On exit, the C matrix is udpated with the addition of AB.
 *          @sa rpk_zgemm_t
 *
 * @param[in] AB
 *          The low-rank structure of the AB matrix to apply to C.
 *
 * @param[in] infomask
 *          Mask of informations returned by the rpk_zxx2lr() functions.
 *          If CORE_LRMM_ORTHOU is set, then AB.u is orthogonal, otherwise an
 *          orthogonalization step is added before adding it to C.
 *
 *******************************************************************************
 *
 * @return The number of flops required to perform the operation.
 *
 *******************************************************************************/
static inline rpk_fixdbl_t
rpkx_zlr2fr( const rpk_ctx_t    *ctx,
             rpk_zgemm_t        *params,
             const rpk_matrix_t *AB,
             int                 infomask )
{
    (void)ctx;
    PASTE_RPK_ZLRMM_PARAMS( params );
    rpk_trans_t      transABu = ( infomask & RAPACK_LRM3_TRANSA ) ? transA : RapackNoTrans;
    rpk_trans_t      transABv = ( infomask & RAPACK_LRM3_TRANSB ) ? transB : RapackNoTrans;
    rpk_int_t        ldabu    = ( transABu == RapackNoTrans ) ? M : AB->rkmax;
    rpk_int_t        ldabv    = ( transABv == RapackNoTrans ) ? AB->rkmax : N;
    rpk_fixdbl_t     flops    = 0.;
    rpk_complex64_t *Cfr      = C->u;
    Cfr += Cm * offy + offx;

    assert( C->rk == -1 );

    /* TODO: find a suitable name to trace this kind of kernel. */
    if ( AB->rk == -1 ) {
        flops = 2 * M * N;
        rpk_zgeadd( transABu, M, N,
                    alpha, AB->u, ldabu,
                    beta,  Cfr,   Cm );
    }
    else {
        flops = flops_zgemm( M, N, AB->rk );
        cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transABu, (CBLAS_TRANSPOSE)transABv,
                     M, N, AB->rk,
                     CBLAS_SADDR(alpha), AB->u, ldabu,
                                         AB->v, ldabv,
                     CBLAS_SADDR(beta),  Cfr,   Cm );
    }

    PASTE_RPK_ZLRMM_VOID;
    return flops;
}

/**
 *******************************************************************************
 *
 * @brief Perform the addition of the low-rank matrix AB and the low-rank
 * matrix C.
 *
 *******************************************************************************
 *
 * @param[in] ctx
 *          The rapack context that defines the compression parameters to use.
 *
 * @param[inout] params
 *          The LRMM structure that stores all the parameters used in the LRMM
 *          functions family.
 *          On exit, the C matrix is udpated with the addition of AB.
 *          @sa rpk_zgemm_t
 *
 * @param[in] AB
 *          The low-rank structure of the AB matrix to apply to C.
 *
 * @param[in] infomask
 *          Mask of informations returned by the rpk_zxx2lr() functions.
 *          If RAPACK_LRM3_ORTHOU is set, then AB.u is orthogonal, otherwise an
 *          orthogonalization step is added before adding it to C.
 *          RAPACK_LRM3_TRANSA and RAPACK_LRM3_TRANSB are used to indicate if
 *          transA and transB need to be applied to AB.u and AB.v respectively.
 *
 *******************************************************************************
 *
 * @return The number of flops required to perform the operation.
 *
 *******************************************************************************/
static inline rpk_fixdbl_t
rpkx_zlr2lr( const rpk_ctx_t    *ctx,
             rpk_zgemm_t        *params,
             const rpk_matrix_t *AB,
             int                 infomask )
{
    PASTE_RPK_ZLRMM_PARAMS( params );
    rpk_int_t    rklimit     = ctx->get_rklimit( ctx, Cm, Cn );
    rpk_int_t    rAB         = ( AB->rk == -1 ) ? rpk_imin( M, N ) : AB->rk;
    rpk_trans_t  transABu    = ( infomask & RAPACK_LRM3_TRANSA ) ? transA : RapackNoTrans;
    rpk_trans_t  transABv    = ( infomask & RAPACK_LRM3_TRANSB ) ? transB : RapackNoTrans;
    rpk_int_t    ldabu       = ( transABu == RapackNoTrans ) ? M : AB->rkmax;
    rpk_int_t    ldabv       = ( transABv == RapackNoTrans ) ? AB->rkmax : N;
    rpk_fixdbl_t total_flops = 0.;
    rpk_fixdbl_t flops       = 0.;

    assert( ( C->rk >= 0 ) && ( C->rk <= C->rkmax ) );

    /*
     * The rank is too big, we need to uncompress/compress C
     */
    if ( ( C->rk + rAB ) > rklimit ) {
        rpk_complex64_t *Cfr, *Coff;
        int              allocated = 0;
        if ( ( Cfr = rpk_zgemm_getws( params, Cm * Cn ) ) == NULL ) {
            Cfr       = malloc( Cm * Cn * sizeof( rpk_complex64_t ) );
            allocated = 1;
        }
        Coff = Cfr;
        Coff += Cm * offy + offx;

        flops += rpk_zlr2ge( RapackNoTrans, Cm, Cn, C, Cfr, Cm );

        /* Add A*B */
        if ( AB->rk == -1 ) {
            rpk_zgeadd( transABu, M, N,
                        alpha, AB->u, ldabu,
                        beta,  Coff,  Cm );
            flops += ( 2. * M * N );
        }
        else {
            cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transABu, (CBLAS_TRANSPOSE)transABv,
                         M, N, AB->rk,
                         CBLAS_SADDR(alpha), AB->u, ldabu,
                                             AB->v, ldabv,
                         CBLAS_SADDR(beta),  Coff,  Cm );
            flops += flops_zgemm( M, N, AB->rk );
        }
        total_flops += flops;

        /* Try to recompress */
        rpk_zlrfree( C );  // TODO: Can we give it directly to ge2lr as this
        flops = ctx->rpk_ge2lr( ctx, -1, Cm, Cn, Cfr, Cm, C );
        total_flops += flops;

        if ( allocated ) {
            free( Cfr );
        }
    }
    /*
     * The rank is not too large, we perform a low-rank update
     */
    else {
        /* Scale C submatrix by beta */
        rpkx_zlrscl( ctx, beta, M, N, Cm, Cn, C, offx, offy );

        /*
         * Transposes AB.u if needed
         * This is a temporary fix as rradd/lrcpy/lrconcatenate do not support
         * transposed AB.u
         * TODO: fix rradd to support transposed AB.u
         */
        if ( infomask & RAPACK_LRM3_TRANSA ) {
            if ( ( transABu != RapackNoTrans ) && ( AB->rk > 0 ) ) {
                rpk_matrix_t    *tmpAB = (rpk_matrix_t *)AB;
                rpk_complex64_t *tmpAu = tmpAB->u;

                rpk_complex64_t *tmp = malloc( M * tmpAB->rkmax * sizeof( rpk_complex64_t ) );
                rpk_zgeadd( transA, M, tmpAB->rk, zone, tmpAB->u, tmpAB->rkmax, zzero, tmp, M );

                tmpAB->u = tmp;
                if ( RAPACK_LRM3_ALLOCU & infomask ) {
                    free( tmpAu );
                }
            }
            infomask &= ~RAPACK_LRM3_TRANSA;
        }

        /* Add A*B */
        assert( !( infomask & RAPACK_LRM3_TRANSA ) );
        total_flops += ctx->rpk_rradd( ctx, RapackNoTrans, transABv, &alpha,
                                       M,  N,  AB,
                                       Cm, Cn, C,
                                       offx, offy );
    }

    PASTE_RPK_ZLRMM_VOID;
    return total_flops;
}

/**
 *******************************************************************************
 *
 * @brief Perform the addition of the low-rank matrix AB into the null matrix C.
 *
 *******************************************************************************
 *
 * @param[in] ctx
 *          The rapack context that defines the compression parameters to use.
 *
 * @param[inout] params
 *          The LRMM structure that stores all the parameters used in the LRMM
 *          functions family.
 *          On exit, the C matrix contains the product AB aligned with its own
 *          dimensions.
 *          @sa rpk_zgemm_t
 *
 * @param[in] AB
 *          The low-rank structure of the AB matrix to apply to C.
 *
 * @param[in] infomask
 *          Mask of informations returned by the rpk_zxx2lr() functions.
 *          If CORE_LRMM_ORTHOU is set, then AB.u is orthogonal, otherwise an
 *          orthogonalization step is added before adding it to C.
 *
 *******************************************************************************
 *
 * @return The number of flops required to perform the operation.
 *
 *******************************************************************************/
static inline rpk_fixdbl_t
rpkx_zlr2null( const rpk_ctx_t    *ctx,
               rpk_zgemm_t        *params,
               const rpk_matrix_t *AB,
               int                 infomask )
{
    PASTE_RPK_ZLRMM_PARAMS( params );
    rpk_int_t    rklimit     = ctx->get_rklimit( ctx, Cm, Cn );
    rpk_trans_t  transABu    = ( infomask & RAPACK_LRM3_TRANSA ) ? transA : RapackNoTrans;
    rpk_trans_t  transABv    = ( infomask & RAPACK_LRM3_TRANSB ) ? transB : RapackNoTrans;
    rpk_int_t    ldabu       = ( transABu == RapackNoTrans ) ? M : AB->rkmax;
    rpk_int_t    ldabv       = ( transABv == RapackNoTrans ) ? AB->rkmax : N;
    rpk_fixdbl_t total_flops = 0.;
    rpk_fixdbl_t flops       = 0.;
    int          allocated   = 0;
    PASTE_RPK_ZLRMM_VOID;

    assert( C->rk == 0 );

    if ( AB->rk > rklimit ) {
        rpk_complex64_t *Cfr, *Coff;
        if ( ( Cfr = rpk_zgemm_getws( params, Cm * Cn ) ) == NULL ) {
            Cfr       = malloc( Cm * Cn * sizeof( rpk_complex64_t ) );
            allocated = 1;
        }
        Coff = Cfr + Cm * offy + offx;

        /* Set to 0 if contribution smaller than C */
        if ( ( M != Cm ) || ( N != Cn ) ) {
            memset( Cfr, 0, Cm * Cn * sizeof( rpk_complex64_t ) );
        }

        /* Uncompress the AB product into C */
        flops = flops_zgemm( M, N, AB->rk );
        cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transABu, (CBLAS_TRANSPOSE)transABv,
                     M, N, AB->rk,
                     CBLAS_SADDR(alpha), AB->u, ldabu,
                                         AB->v, ldabv,
                     CBLAS_SADDR(beta),  Coff,  Cm );
        total_flops += flops;

        /* Try to recompress C */
        flops = ctx->rpk_ge2lr( ctx, -1, Cm, Cn, Cfr, Cm, C );
        total_flops += flops;

        if ( allocated ) {
            free( work );
        }
    }
    else {
        /*
         * Let's chech that AB->u is orthogonal before copying it to C.u
         */
        int orthou = infomask & RAPACK_LRM3_ORTHOU;
        if ( !orthou ) {
            rpk_complex64_t *ABfr;
            rpk_matrix_t     backup;
            int              allocated = 0;

            if ( AB->rk > 0 ) {
                if ( ( ABfr = rpk_zgemm_getws( params, M * N ) ) == NULL ) {
                    ABfr      = malloc( M * N * sizeof( rpk_complex64_t ) );
                    allocated = 1;
                }

                cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transABu, (CBLAS_TRANSPOSE)transABv,
                             M, N, AB->rk,
                             CBLAS_SADDR(zone),  AB->u, ldabu,
                                                 AB->v, ldabv,
                             CBLAS_SADDR(zzero), ABfr,  M );
                flops = flops_zgemm( M, N, AB->rk );
            }
            else {
                ABfr  = AB->u;
                flops = 0.0;
            }

            flops += ctx->rpk_ge2lr( ctx, rklimit, M, N, ABfr, M, &backup );

            rpkx_zlrcpy( ctx, RapackNoTrans, alpha,
                         M, N, &backup, Cm, Cn, C,
                         offx, offy );

            rpk_zlrfree( &backup );
            total_flops += flops;

            if ( allocated ) {
                free( ABfr );
            }
        }
        /*
         * AB->u is orthogonal, we directly copy AB->u into C
         */
        else {
            assert( transABu == RapackNoTrans );
            rpkx_zlrcpy( ctx, transABv, alpha,
                         M, N, AB, Cm, Cn, C,
                         offx, offy );
        }
    }

    return total_flops;
}

/**
 *******************************************************************************
 *
 * @brief Perform the addition of two low-rank matrices.
 *
 *******************************************************************************
 *
 * @param[in] ctx
 *          The rapack context that defines the compression parameters to use.
 *
 * @param[inout] params
 *          The LRMM structure that stores all the parameters used in the LRMM
 *          functions family.
 *          On exit, the C matrix contains the addition of C and A aligned with its own
 *          dimensions.
 *          @sa rpk_zgemm_t
 *
 * @param[in] A
 *          The low-rank structure of the A matrix to add to C.
 *
 * @param[in] infomask
 *          Mask of informations returned by the rpk_zxx2lr() functions.
 *          If CORE_LRMM_ORTHOU is set, then A.u is orthogonal, otherwise an
 *          orthogonalization step is added before adding it to C.
 *
 *******************************************************************************
 *
 * @return The number of flops required to perform the operation.
 *
 *******************************************************************************/
rpk_fixdbl_t
rpkx_zlradd( const rpk_ctx_t    *ctx,
             rpk_zgemm_t        *params,
             const rpk_matrix_t *AB,
             int                 infomask )
{
    PASTE_RPK_ZLRMM_PARAMS( params );
    rpk_fixdbl_t  flops = 0.;

    if ( AB->rk != 0 ) {
        ctx->lock( ctx, params->lock );
        switch ( C->rk ) {
            case -1:
                /*
                 * C became full rank
                 */
                flops = rpkx_zlr2fr( ctx, params, AB, infomask );
                break;

            case 0:
                /*
                 * C is still null
                 */
                flops = rpkx_zlr2null( ctx, params, AB, infomask );
                break;

            default:
                /*
                 * C is low-rank of rank k
                 */
                flops = rpkx_zlr2lr( ctx, params, AB, infomask );
        }
        assert( C->rk <= C->rkmax );
        ctx->unlock( ctx, params->lock );
    }
    else {
        flops += rpkx_zlrscl( ctx, beta, M, N, Cm, Cn, C, offx, offy );
    }

    PASTE_RPK_ZLRMM_VOID;
    return flops;
}
