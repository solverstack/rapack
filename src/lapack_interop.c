/**
 *
 * @file lapack_interop.c
 *
 * RAPACK-LAPACK interopability functions and constants
 *
 * @copyright 2024-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @comment Parts of this file have been initially extracted from CHAMELEON 1.2.1
 * @author Abel Calluaud
 * @date 2024-02-08
 *
 **/


/**
 *  LAPACK Constants
 */
char *rpk_lapack_consts[] =
{
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "",                     // 100
    "Row",                  // 101: ChamRowMajor
    "Column",               // 102: ChamColMajor
    "", "", "", "", "", "", "", "",
    "No transpose",         // 111: ChamNoTrans
    "Transpose",            // 112: ChamTrans
    "Conjugate transpose",  // 113: ChamConjTrans
    "", "", "", "", "", "", "",
    "Upper",                // 121: ChamUpper
    "Lower",                // 122: ChamLower
    "All",                  // 123: ChamUpperLower
    "", "", "", "", "", "", "",
    "Non-unit",             // 131: ChamNonUnit
    "Unit",                 // 132: ChamUnit
    "", "", "", "", "", "", "", "",
    "Left",                 // 141: ChamLeft
    "Right",                // 142: ChamRight
    "", "", "", "", "", "", "", "",
    "",                     // 151:
    "",                     // 152:
    "",                     // 153:
    "",                     // 154:
    "",                     // 155:
    "",                     // 156:
    "Epsilon",              // 157: ChamEps
    "",                     // 158:
    "",                     // 159:
    "",                     // 160:
    "", "", "", "", "", "", "", "", "", "",
    "One norm",             // 171: ChamOneNorm
    "",                     // 172: ChamRealOneNorm
    "",                     // 173: ChamTwoNorm
    "Frobenius norm",       // 174: ChamFrobeniusNorm
    "Infinity norm",        // 175: ChamInfNorm
    "",                     // 176: ChamRealInfNorm
    "Maximum norm",         // 177: ChamMaxNorm
    "",                     // 178: ChamRealMaxNorm
    "",                     // 179
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "",                     // 200
    "Uniform",              // 201: ChamDistUniform
    "Symmetric",            // 202: ChamDistSymmetric
    "Normal",               // 203: ChamDistNormal
    "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "",                     // 240
    "Hermitian",            // 241 ChamHermGeev
    "Positive ev Hermitian",// 242 ChamHermPoev
    "NonSymmetric pos sv",  // 243 ChamNonsymPosv
    "Symmetric pos sv",     // 244 ChamSymPosv
    "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "",                     // 290
    "No Packing",           // 291 ChamNoPacking
    "U zero out subdiag",   // 292 ChamPackSubdiag
    "L zero out superdiag", // 293 ChamPackSupdiag
    "C",                    // 294 ChamPackColumn
    "R",                    // 295 ChamPackRow
    "B",                    // 296 ChamPackLowerBand
    "Q",                    // 297 ChamPackUpeprBand
    "Z",                    // 298 ChamPackAll
    "",                     // 299

    "",                     // 300
    "No vectors",           // 301 ChamNoVec
    "Vectors needed",       // 302 ChamVec, ChamSVDvrange
    "I",                    // 303 ChamIvec, ChamSVDirange
    "A",                    // 304 ChamAllVec, ChamSVDall
    "S",                    // 305 ChamSVec
    "O",                    // 306 ChamOVec
    "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "", "", "", "", "", "", "", "", "", "",
    "",                     // 390
    "Forward",              // 391
    "Backward",             // 392
    "", "", "", "", "", "", "", "",
    "Columnwise",           // 401
    "Rowwise",              // 402
    "", "", "", "", "", "", "", ""  // Remember to add a coma!
};
