/**
 *
 * @file rpk_zxx2fr.c
 *
 * RAPACK low-rank kernel routines that form the product of two matrices A and B
 * into a low-rank form for an update on a full rank matrix.
 *
 * @copyright 2016-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Gregoire Pichon
 * @author Pierre Ramet
 * @author Abel Calluaud
 * @date 2024-03-11
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"
#ifndef DOXYGEN_SHOULD_SKIP_THIS
static rpk_complex64_t zone  = 1.0;
static rpk_complex64_t zzero = 0.0;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

/**
 *******************************************************************************
 *
 * @brief Perform the full-rank operation C = alpha * op(A) * op(B) + beta C
 *
 *******************************************************************************
 *
 * @param[inout] params
 *          The LRMM structure that stores all the parameters used in the LRMM
 *          functions family.
 *          On exit, the C matrix contains the product AB aligned with its own
 *          dimensions.
 *          @sa rpk_zgemm_t
 *
 *******************************************************************************
 *
 * @return The number of flops required to perform the operation.
 *
 *******************************************************************************/
rpk_fixdbl_t
rpkx_zfrfr2fr( const rpk_ctx_t *ctx, rpk_zgemm_t *params )
{
    rpk_int_t        ldau, ldbu, ldcu;
    rpk_complex64_t *Cptr;
    rpk_fixdbl_t     flops;
    PASTE_RPK_ZLRMM_PARAMS( params );
    ldau = ( transA == RapackNoTrans ) ? M : K;
    ldbu = ( transB == RapackNoTrans ) ? K : N;
    ldcu = Cm;

    Cptr = C->u;
    Cptr += ldcu * offy + offx;

    ctx->lock( ctx, lock );
    assert( C->rk == -1 ); /* Check that C has not changed due to parallelism */

    /*
     * Everything is full rank we apply directly a GEMM
     */
    cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, (CBLAS_TRANSPOSE)transB,
                 M, N, K,
                 CBLAS_SADDR(alpha), A->u, ldau,
                                     B->u, ldbu,
                 CBLAS_SADDR(beta),  Cptr, ldcu );
    flops = flops_zgemm( M, N, K );

    ctx->unlock( ctx, lock );

    PASTE_RPK_ZLRMM_VOID;
    return flops;
}

/**
 *******************************************************************************
 *
 * @brief Perform the operation C = alpha * op(A) * op(B) + beta C, with A and C
 * full-rank and B low-rank.
 *
 *******************************************************************************
 *
 * @param[inout] params
 *          The LRMM structure that stores all the parameters used in the LRMM
 *          functions family.
 *          On exit, the C matrix contains the product AB aligned with its own
 *          dimensions.
 *          @sa rpk_zgemm_t
 *
 *******************************************************************************
 *
 * @return The number of flops required to perform the operation.
 *
 *******************************************************************************/
rpk_fixdbl_t
rpkx_zfrlr2fr( const rpk_ctx_t *ctx, rpk_zgemm_t *params )
{
    PASTE_RPK_ZLRMM_PARAMS( params );
    rpk_complex64_t *Cptr;
    rpk_int_t        ldau, ldcu;
    rpk_fixdbl_t     flops1 = flops_zgemm( M, B->rk, K ) + flops_zgemm( M, N, B->rk );
    rpk_fixdbl_t     flops2 = flops_zgemm( K, N, B->rk ) + flops_zgemm( M, N, K );
    rpk_fixdbl_t     flops;
    int              allocated = 0;
    rpk_complex64_t *Bu;
    rpk_int_t        ldBu;
    rpk_complex64_t *Bv;
    rpk_int_t        ldBv;
    PASTE_RPK_ZLRMM_VOID;

    if ( transB == RapackNoTrans ) {
        Bu   = B->u;
        ldBu = K;
        Bv   = B->v;
        ldBv = B->rkmax;
    }
    else {
        Bu   = B->v;
        ldBu = B->rkmax;
        Bv   = B->u;
        ldBv = N;
    }

    ldau = ( transA == RapackNoTrans ) ? M : K;

    ldcu = Cm;
    Cptr = C->u;
    Cptr += ldcu * offy + offx;

    /*
     *  A(M-by-K) * B( N-by-rb x rb-by-K )^t
     */
    if ( flops1 <= flops2 ) {
        if ( ( work = rpk_zgemm_getws( params, M * B->rk ) ) == NULL ) {
            work      = malloc( M * B->rk * sizeof( rpk_complex64_t ) );
            allocated = 1;
        }

        /*
         *  (A * Bv) * Bu^t
         */
        cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, (CBLAS_TRANSPOSE)transB,
                     M, B->rk, K,
                     CBLAS_SADDR(zone),  A->u, ldau,
                                         Bu,   ldBu,
                     CBLAS_SADDR(zzero), work, M );

        ctx->lock( ctx, lock );
        assert( C->rk == -1 ); /* Check that C has not changed due to parallelism */
        cblas_zgemm( CblasColMajor, CblasNoTrans, (CBLAS_TRANSPOSE)transB,
                     M, N, B->rk,
                     CBLAS_SADDR(alpha), work, M,
                                         Bv,   ldBv,
                     CBLAS_SADDR(beta),  Cptr, ldcu );
        flops = flops1;
        ctx->unlock( ctx, lock );
    }
    else {
        if ( ( work = rpk_zgemm_getws( params, K * N ) ) == NULL ) {
            work      = malloc( K * N * sizeof( rpk_complex64_t ) );
            allocated = 1;
        }

        /*
         *  A * (Bu * Bv^t)^t
         */
        cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transB, (CBLAS_TRANSPOSE)transB,
                     K, N, B->rk,
                     CBLAS_SADDR(zone),  Bu,   ldBu,
                                         Bv,   ldBv,
                     CBLAS_SADDR(zzero), work, K );

        ctx->lock( ctx, lock );
        assert( C->rk == -1 ); /* Check that C has not changed due to parallelism */
        cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, CblasNoTrans,
                     M, N, K,
                     CBLAS_SADDR(alpha), A->u, ldau,
                                         work, K,
                     CBLAS_SADDR(beta),  Cptr, ldcu );

        flops = flops2;
        ctx->unlock( ctx, lock );
    }

    if ( allocated ) {
        free( work );
    }
    return flops;
}

/**
 *******************************************************************************
 *
 * @brief Perform the operation C = alpha * op(A) * op(B) + beta C, with B and C
 * full-rank and A low-rank.
 *
 *******************************************************************************
 *
 * @param[inout] params
 *          The LRMM structure that stores all the parameters used in the LRMM
 *          functions family.
 *          On exit, the C matrix contains the product AB aligned with its own
 *          dimensions.
 *          @sa rpk_zgemm_t
 *
 *******************************************************************************
 *
 * @return The number of flops required to perform the operation.
 *
 *******************************************************************************/
rpk_fixdbl_t
rpkx_zlrfr2fr( const rpk_ctx_t *ctx, rpk_zgemm_t *params )
{
    PASTE_RPK_ZLRMM_PARAMS( params );
    rpk_complex64_t *Cptr;
    rpk_int_t        ldbu, ldcu;
    rpk_fixdbl_t     flops1 = flops_zgemm( A->rk, N, K ) + flops_zgemm( M, N, A->rk );
    rpk_fixdbl_t     flops2 = flops_zgemm( M, K, A->rk ) + flops_zgemm( M, N, K );
    rpk_fixdbl_t     flops;
    int              allocated = 0;
    rpk_complex64_t *Au;
    rpk_int_t        ldAu;
    rpk_complex64_t *Av;
    rpk_int_t        ldAv;
    PASTE_RPK_ZLRMM_VOID;

    assert( A->rk > 0 );

    if ( transA == RapackNoTrans ) {
        Au   = A->u;
        ldAu = M;
        Av   = A->v;
        ldAv = A->rkmax;
    }
    else {
        Au   = A->v;
        ldAu = A->rkmax;
        Av   = A->u;
        ldAv = K;
    }

    ldbu = ( transB == RapackNoTrans ) ? K : N;
    ldcu = Cm;
    Cptr = C->u;
    Cptr += ldcu * offy + offx;

    /*
     *  A( M-by-ra x ra-by-K ) * B(N-by-K)^t
     */
    if ( flops1 <= flops2 ) {
        if ( ( work = rpk_zgemm_getws( params, A->rk * N ) ) == NULL ) {
            work      = malloc( A->rk * N * sizeof( rpk_complex64_t ) );
            allocated = 1;
        }

        /*
         *  Au * (Av^t * B^t)
         */
        cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, (CBLAS_TRANSPOSE)transB,
                     A->rk, N, K,
                     CBLAS_SADDR(zone),  Av,   ldAv,
                                         B->u, ldbu,
                     CBLAS_SADDR(zzero), work, A->rk );

        ctx->lock( ctx, lock );
        assert( C->rk == -1 ); /* Check that C has not changed due to parallelism */
        cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, CblasNoTrans,
                     M, N, A->rk,
                     CBLAS_SADDR(alpha), Au,   ldAu,
                                         work, A->rk,
                     CBLAS_SADDR(beta),  Cptr, ldcu );

        flops = flops1;
        ctx->unlock( ctx, lock );
    }
    else {
        if ( ( work = rpk_zgemm_getws( params, M * K ) ) == NULL ) {
            work      = malloc( M * K * sizeof( rpk_complex64_t ) );
            allocated = 1;
        }

        /*
         *  (Au * Av^t) * B^t
         */
        cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, (CBLAS_TRANSPOSE)transA,
                     M, K, A->rk,
                     CBLAS_SADDR(zone),  Au,   ldAu,
                                         Av,   ldAv,
                     CBLAS_SADDR(zzero), work, M );

        ctx->lock( ctx, lock );
        assert( C->rk == -1 ); /* Check that C has not changed due to parallelism */
        cblas_zgemm( CblasColMajor, CblasNoTrans, (CBLAS_TRANSPOSE)transB,
                     M, N, K,
                     CBLAS_SADDR(alpha), work, M,
                                         B->u, ldbu,
                     CBLAS_SADDR(beta),  Cptr, ldcu );

        flops = flops2;
        ctx->unlock( ctx, lock );
    }

    if ( allocated ) {
        free( work );
    }
    return flops;
}

/**
 *******************************************************************************
 *
 * @brief Perform the operation C = alpha * op(A) * op(B) + beta C, with A and B
 * low-rank and C full-rank.
 *
 *******************************************************************************
 *
 * @param[inout] params
 *          The LRMM structure that stores all the parameters used in the LRMM
 *          functions family.
 *          On exit, the C matrix contains the product AB aligned with its own
 *          dimensions.
 *          @sa rpk_zgemm_t
 *
 *******************************************************************************
 *
 * @return The number of flops required to perform the operation.
 *
 *******************************************************************************/
rpk_fixdbl_t
rpkx_zlrlr2fr( const rpk_ctx_t *ctx, rpk_zgemm_t *params )
{
    PASTE_RPK_ZLRMM_PARAMS( params );
    rpk_complex64_t *Cptr;
    rpk_int_t        ldcu;
    rpk_matrix_t     AB;
    int              infomask = 0;
    rpk_fixdbl_t     flops;

    ldcu = Cm;
    Cptr = C->u;
    Cptr += ldcu * offy + offx;

    flops = rpkx_zlrlr2lr( ctx, params, &AB, &infomask );
    assert( AB.rk != -1 );
    assert( AB.rkmax != -1 );

    if ( AB.rk > 0 ) {
        rpk_trans_t transABu = ( infomask & RAPACK_LRM3_TRANSA ) ? transA : RapackNoTrans;
        rpk_trans_t transABv = ( infomask & RAPACK_LRM3_TRANSB ) ? transB : RapackNoTrans;
        rpk_int_t   ldabu    = ( transABu == RapackNoTrans ) ? M : AB.rkmax;
        rpk_int_t   ldabv    = ( transABv == RapackNoTrans ) ? AB.rkmax : N;

        ctx->lock( ctx, lock );
        assert( C->rk == -1 ); /* Check that C has not changed due to parallelism */
        cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transABu, (CBLAS_TRANSPOSE)transABv,
                     M, N, AB.rk,
                     CBLAS_SADDR(alpha), AB.u, ldabu,
                                         AB.v, ldabv,
                     CBLAS_SADDR(beta),  Cptr, ldcu );
        flops = flops_zgemm( M, N, AB.rk );
        ctx->unlock( ctx, lock );
    }

    /* Free memory from zlrm3 */
    if ( infomask & RAPACK_LRM3_ALLOCU ) {
        free( AB.u );
    }
    if ( infomask & RAPACK_LRM3_ALLOCV ) {
        free( AB.v );
    }

    PASTE_RPK_ZLRMM_VOID;
    return flops;
}
