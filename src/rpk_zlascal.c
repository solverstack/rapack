/**
 *
 * @file rpk_zlascal.c
 *
 * @copyright 2009-2016 The University of Tennessee and The University of
 *                      Tennessee Research Foundation. All rights reserved.
 * @copyright 2012-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 * @copyright 2016-2018 KAUST. All rights reserved.
 *
 *
 * @brief RAPACK rpk_zlascal CPU kernel
 *
 * @version 1.0.0
 * @comment This file has initially been extracted from CHAMELEON 1.2.1
 * @author Dalal Sukkari
 * @author Mathieu Faverge
 * @author Abel Calluaud
 * @date 2024-02-08
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

/**
 *******************************************************************************
 *
 * @ingroup CORE_rpk_complex64_t
 *
 *  CORE_zlascal scales a two-dimensional matrix A. As opposite to
 *  CORE_zlascl(), no checks is performed to prevent under/overflow. This should
 *  have been done at higher level.
 *
 *******************************************************************************
 *
 * @param[in] uplo
 *          Specifies the shape of A:
 *          = RapackUpperLower: A is a general matrix.
 *          = RapackUpper: A is an upper trapezoidal matrix.
 *          = RapackLower: A is a lower trapezoidal matrix.
 *
 * @param[in] m is the number of rows of the matrix A. m >= 0
 *
 * @param[in] n is the number of columns of the matrix A. n >= 0
 *
 * @param[in] alpha
 *            The scalar factor.
 *
 * @param[in,out] A is the matrix to be multiplied by alpha
 *
 * @param[in] lda is the leading dimension of the array A. lda >= max(1,m).
 *
 *******************************************************************************
 *
 * @retval CHAMELEON_SUCCESS successful exit
 * @retval <0 if -i, the i-th argument had an illegal value
 *
 */
rpk_int_t
rpk_zlascal( rpk_uplo_t uplo, rpk_int_t m, rpk_int_t n,
             rpk_complex64_t alpha, rpk_complex64_t *A, rpk_int_t lda )
{
    int i;

    if ( (uplo != RapackUpperLower) &&
         (uplo != RapackUpper)      &&
         (uplo != RapackLower))
    {
        fprintf(stderr, "illegal value of uplo");
        return -1;
    }

    if (m < 0) {
        fprintf(stderr, "Illegal value of m");
        return -2;
    }
    if (n < 0) {
        fprintf(stderr, "Illegal value of n");
        return -3;
    }
    if ( (lda < rpk_imax(1,m)) && (m > 0) ) {
        fprintf(stderr, "Illegal value of lda");
        return -6;
    }

    switch ( uplo ) {
    case RapackUpper:
        for(i=0; i<n; i++) {
            cblas_zscal( rpk_imin( i+1, m ), CBLAS_SADDR(alpha), A+i*lda, 1 );
        }
        break;

    case RapackLower:
        for(i=0; i<n; i++) {
            cblas_zscal( rpk_imax( m, m-i ), CBLAS_SADDR(alpha), A+i*lda, 1 );
        }
        break;
    default:
        if (m == lda) {
            cblas_zscal( m*n, CBLAS_SADDR(alpha), A, 1 );
        }
        else {
            for(i=0; i<n; i++) {
                cblas_zscal( m, CBLAS_SADDR(alpha), A+i*lda, 1 );
            }
        }
    }

    return 0;
}
