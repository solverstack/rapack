/**
 *
 * @file frobeniusupdate.h
 *
 * Formula to update frobenius norm computation in a safe manner.
 *
 * @copyright 2004-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Esragul Korkmaz
 * @author Tony Delarue
 * @date 2023-12-14
 *
 */
#ifndef _frobeniusupdate_h_
#define _frobeniusupdate_h_

/**
 *******************************************************************************
 *
 * @ingroup rapack_internal
 *
 * @brief Update the couple (scale, sumsq) with one element when computing the
 * Frobenius norm.
 *
 * The frobenius norm is equal to scale * sqrt( sumsq ), this method allows to
 * avoid overflow in the sum square computation.
 *
 *******************************************************************************
 *
 * @param[inout] scale
 *           On entry, the former scale
 *           On exit, the update scale to take into account the value
 *
 * @param[inout] sumsq
 *           On entry, the former sumsq
 *           On exit, the update sumsq to take into account the value
 *
 * @param[in] value
 *          The value to integrate into the couple (scale, sumsq)
 *
 *******************************************************************************/
#if defined(PRECISION_d) || defined(PRECISION_z)
static inline void
rpk_dfrobenius_update( int nb, double *scale, double *sumsq, const double *value )
{
    double absval = fabs( *value );
    if ( absval != 0. ) {
        double ratio;
        if ( ( *scale ) < absval ) {
            ratio  = ( *scale ) / absval;
            *sumsq = (double)nb + ( *sumsq ) * ratio * ratio;
            *scale = absval;
        }
        else {
            ratio  = absval / ( *scale );
            *sumsq = ( *sumsq ) + (double)nb * ratio * ratio;
        }
    }
}

/**
 *******************************************************************************
 *
 * @ingroup kernel_lr_svd_null
 *
 * @brief Compute the frobenius norm of a vector
 *
 * This routine is inspired from LAPACK zlassq function, and allows to
 * accumulate the contribution backward for better accuracy as opposed to dnrm2
 * which allows only positive increment.
 *
 *******************************************************************************
 *
 * @param[in] n
 *          The number of elemnts in the vector
 *
 * @param[in] x
 *          The vector of size n * incx
 *
 * @param[in] incx
 *          The increment between two elments in the vector x.
 *
 *******************************************************************************
 *
 * @return  The frobenius norm of the vector x.
 *
 *******************************************************************************/
static inline double
rpk_dlassq( int n, const double *x, int incx )
{
    double scale = 1.;
    double sumsq = 0.;
    int    i;

    for ( i = 0; i < n; i++, x += incx ) {
        rpk_dfrobenius_update( 1, &scale, &sumsq, x );
    }

    return scale * sqrt( sumsq );
}

#elif defined(PRECISION_s) || defined(PRECISION_c)

static inline void
rpk_sfrobenius_update( int nb, float *scale, float *sumsq, const float *value )
{
    float absval = fabs( *value );
    if ( absval != 0. ) {
        float ratio;
        if ( ( *scale ) < absval ) {
            ratio  = ( *scale ) / absval;
            *sumsq = (float)nb + ( *sumsq ) * ratio * ratio;
            *scale = absval;
        }
        else {
            ratio  = absval / ( *scale );
            *sumsq = ( *sumsq ) + (float)nb * ratio * ratio;
        }
    }
}

static inline float
rpk_slassq( int n, const float *x, int incx )
{
    float scale = 1.;
    float sumsq = 0.;
    int   i;

    for ( i = 0; i < n; i++, x += incx ) {
        rpk_sfrobenius_update( 1, &scale, &sumsq, x );
    }

    return scale * sqrt( sumsq );
}

#endif /* defined(PRECISION_s) || defined(PRECISION_c) */

#endif /* _frobeniusupdate_h_ */
