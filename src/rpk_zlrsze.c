/**
 *
 * @file rpk_zlrsze.c
 *
 * RAPACK low-rank kernel routines
 *
 * @copyright 2016-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Gregoire Pichon
 * @author Esragul Korkmaz
 * @author Mathieu Faverge
 * @author Pierre Ramet
 * @author Nolan Bredel
 * @date 2024-01-08
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

/**
 *******************************************************************************
 *
 * @brief Resize a low-rank matrix
 *
 *******************************************************************************
 *
 * @param[in] copy
 *          Enable/disable the copy of the data from A->u and A->v into the new
 *          low-rank representation.
 *
 * @param[in] M
 *          The number of rows of the matrix A.
 *
 * @param[in] N
 *          The number of columns of the matrix A.
 *
 * @param[inout] A
 *          The low-rank representation of the matrix. At exit, this structure
 *          is modified with the new low-rank representation of A, is the rank
 *          is small enough
 *
 * @param[in] newrk
 *          The new rank of the matrix A.
 *
 * @param[in] newrkmax
 *          The new maximum rank of the matrix A. Useful if the low-rank
 *          structure was allocated with more data than the rank.
 *
 * @param[in] rklimit
 *          The maximum rank to store the matrix in low-rank format. If
 *          -1, set to rpk_get_rklimit(M, N)
 *
 *******************************************************************************
 *
 * @return  The new rank of A
 *
 *******************************************************************************/
int
rpkx_zlrsze( const rpk_ctx_t *ctx,
             int              copy,
             rpk_int_t        M,
             rpk_int_t        N,
             rpk_matrix_t    *A,
             rpk_int_t        newrk,
             rpk_int_t        newrkmax,
             rpk_int_t        rklimit )
{
    /* If no limit on the rank is given, let's take min(M, N) */
    rklimit = ( rklimit == -1 ) ? ctx->get_rklimit( ctx, M, N ) : rklimit;

    /* If no extra memory allocated, let's fix rkmax to rk */
    newrkmax = ( newrkmax == -1 ) ? newrk : newrkmax;
    newrkmax = rpk_imax( newrkmax, newrk );

    /*
     * It is not interesting to compress, so we alloc space to store the full matrix
     */
    if ( ( newrk > rklimit ) || ( newrk == -1 ) ) {
        A->u = realloc( A->u, M * N * sizeof(rpk_complex64_t) );
#if defined(RAPACK_DEBUG_LR)
        free( A->v );
#endif
        A->v     = NULL;
        A->rk    = -1;
        A->rkmax = M;
        return -1;
    }
    /*
     * The rank is null, we free everything
     */
    else if ( newrkmax == 0 ) {
        /*
         * The rank is null, we free everything
         */
        free( A->u );
#if defined(RAPACK_DEBUG_LR)
        free( A->v );
#endif
        A->u     = NULL;
        A->v     = NULL;
        A->rkmax = newrkmax;
        A->rk    = newrk;
    }
    /*
     * The rank is non null, we allocate the correct amount of space, and
     * compress the stored information if necessary
     */
    else {
        rpk_complex64_t *u, *v;
        int              ret;

        if (  ( A->rk == -1 ) ||
             (( A->rk != -1 ) && ( newrkmax != A->rkmax  )) )
        {
#if defined(RAPACK_DEBUG_LR)
            u = malloc( M * newrkmax * sizeof(rpk_complex64_t) );
            v = malloc( N * newrkmax * sizeof(rpk_complex64_t) );
#else
            u = malloc( ( M + N ) * newrkmax * sizeof(rpk_complex64_t) );
            v = u + M * newrkmax;
#endif
            if ( copy ) {
                assert( A->rk != -1 );
                ret = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'A', M, newrk,
                                           A->u, M, u, M );
                assert( ret == 0 );
                ret = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'A', newrk, N,
                                           A->v, A->rkmax, v, newrkmax );
                assert( ret == 0 );
            }
            free( A->u );
#if defined(RAPACK_DEBUG_LR)
            free( A->v );
#endif
            A->u = u;
            A->v = v;
        }

        /* Update rk and rkmax */
        A->rkmax = newrkmax;
        A->rk    = newrk;

        (void)ret;
    }
    assert( A->rk <= A->rkmax );
    return 0;
}
