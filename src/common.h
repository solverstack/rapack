/**
 *
 * @file common.h
 *
 * @copyright 2004-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Pierre Ramet
 * @author Tony Delarue
 * @author Florent Pruvost
 * @author Abel Calluaud
 * @date 2024-02-08
 *
 **/
#ifndef _rapack_common_h_
#define _rapack_common_h_

#include "rapack.h"
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <inttypes.h>
#include <limits.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <cblas.h>
#include <lapacke.h>
#include "flops.h"

/**
 * @brief Macro to specify if the U part of a low-rank matrix is orthogonal or not (Used in LRMM
 * functions).
 */
#define RAPACK_LRM3_ORTHOU ( 1 << 0 )
/**
 * @brief Macro to specify if the U part of a low-rank matrix has been allocated and need to be
 * freed or not (Used in LRMM functions).
 */
#define RAPACK_LRM3_ALLOCU ( 1 << 1 )
/**
 * @brief Macro to specify if the V part of a low-rank matrix has been allocated and need to be
 * freed or not (Used in LRMM functions).
 */
#define RAPACK_LRM3_ALLOCV ( 1 << 2 )
/**
 * @brief Macro to specify if the the operator on A, still needs to be applied to the U part of the
 * low-rank matrix or not (Used in LRMM functions).
 */
#define RAPACK_LRM3_TRANSA ( 1 << 3 )
/**
 * @brief Macro to specify if the the operator on B, still needs to be applied to the V part of the
 * low-rank matrix or not (Used in LRMM functions).
 */
#define RAPACK_LRM3_TRANSB ( 1 << 4 )

/********************************************************************
 * Errors functions
 */
#if defined( __GNUC__ )
static inline void rapack_print_error( const char *fmt, ... )
    __attribute__( ( format( printf, 1, 2 ) ) );
static inline void rapack_print_warning( const char *fmt, ... )
    __attribute__( ( format( printf, 1, 2 ) ) );
#endif

static inline void
rapack_print_error( const char *fmt, ... )
{
    va_list arglist;
    va_start( arglist, fmt );
    fprintf( stderr, "ERROR: " );
    vfprintf( stderr, fmt, arglist );
    va_end( arglist );
}

static inline void
rapack_print_warning( const char *fmt, ... )
{
    va_list arglist;
    va_start( arglist, fmt );
    fprintf( stderr, "WARNING: " );
    vfprintf( stderr, fmt, arglist );
    va_end( arglist );
}

/********************************************************************
 * CBLAS value address
 */
#ifndef CBLAS_SADDR
#define CBLAS_SADDR( a_ ) ( &( a_ ) )
#endif

#endif /* _rapack_common_h_ */
