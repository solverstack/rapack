/**
 *
 * @file rpk_zlrnrm.c
 *
 * RAPACK low-rank kernel to compute the norms of a low-rank block.
 *
 * @copyright 2016-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @date 2023-12-14
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

/**
 *******************************************************************************
 *
 * @brief Compute the norm of a low-rank matrix.
 *
 *******************************************************************************
 *
 * @param[in] ntype
 *          The matrix norm to compute.
 *
 * @param[in] transV
 *          TODO
 *
 * @param[in] M
 *          TODO
 *
 * @param[in] N
 *          TODO
 *
 * @param[in] A
 *          The low-rank matrix
 *
 *******************************************************************************
 *
 * @return The norm of the matrix A
 *
 *******************************************************************************/
double
rpk_zlrnrm( rpk_normtype_t ntype, int transV, rpk_int_t M, rpk_int_t N, const rpk_matrix_t *A )
{
    if ( ntype != RapackFrobeniusNorm ) {
        fprintf( stderr, "rpk_zlrnrm: Only the Frobenius norm is available for now\n" );
        ntype = RapackFrobeniusNorm;
    }

    if ( A->rk == -1 ) {
        assert( transV == RapackNoTrans );
        return LAPACKE_zlange_work( LAPACK_COL_MAJOR, 'f',
                                    M, N, A->u, M, NULL );
    }
    else if ( A->rk == 0 ) {
        return 0.;
    }
    else {
        double normU, normV;

        normU = LAPACKE_zlange_work( LAPACK_COL_MAJOR, 'f',
                                     M, A->rk, A->u, M, NULL );
        if ( transV == RapackNoTrans ) {
            normV = LAPACKE_zlange_work( LAPACK_COL_MAJOR, 'f',
                                         A->rk, N, A->v, A->rkmax, NULL );
        }
        else {
            normV = LAPACKE_zlange_work( LAPACK_COL_MAJOR, 'f',
                                         N, A->rk, A->v, N, NULL );
        }
        /* This is an over-estimation of the norm as with frobenius ||UV|| <= ||U|| * ||V|| */
        return normU * normV;
    }
}
