/**
 *
 * @file rpk_zrradd_qr.c
 *
 * RAPACK low-rank kernel routines
 *
 * @copyright 2016-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Gregoire Pichon
 * @author Esragul Korkmaz
 * @author Mathieu Faverge
 * @author Pierre Ramet
 * @author Nolan Bredel
 * @author Abel Calluaud
 * @date 2024-02-08
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

#ifndef DOXYGEN_SHOULD_SKIP_THIS
static rpk_complex64_t zone  = 1.0;
static rpk_complex64_t zzero = 0.0;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

/**
 *******************************************************************************
 *
 * @brief Template to perform the addition of two low-rank structures with
 * compression kernel based on QR decomposition.
 *
 * Add two LR structures A=(-u1) v1^T and B=u2 v2^T into u2 v2^T
 *
 *    u2v2^T - u1v1^T = (u2 u1) (v2 v1)^T
 *    Orthogonalize (u2 u1) = (u2, u1 - u2(u2^T u1)) * (I u2^T u1)
 *                                                     (0    I   )
 *    Compute Rank Revealing QR decomposition of (I u2^T u1) * (v2 v1)^T
 *                                               (0    I   )
 * Any QR rank revealing kernel can be used for the recompression of the V part.
 *
 *******************************************************************************
 *
 * @param[in] rrqrfct
 *          QR decomposition function used to compute the rank revealing
 *          factorization of the sum of the two low-rank matrices.
 *
 * @param[in] ctx
 *          The structure with low-rank parameters.
 *
 * @param[in] transAv
 *         @arg RapackNoTrans:  No transpose, op( A ) = A;
 *         @arg RapackTrans:  Transpose, op( A ) = A';
 *
 * @param[in] alphaptr
 *          alpha * A is add to B
 *
 * @param[in] M1
 *          The number of rows of the matrix A.
 *
 * @param[in] N1
 *          The number of columns of the matrix A.
 *
 * @param[in] A
 *          The low-rank representation of the matrix A.
 *
 * @param[in] M2
 *          The number of rows of the matrix B.
 *
 * @param[in] N2
 *          The number of columns of the matrix B.
 *
 * @param[in] B
 *          The low-rank representation of the matrix B.
 *
 * @param[in] offx
 *          The horizontal offset of A with respect to B.
 *
 * @param[in] offy
 *          The vertical offset of A with respect to B.
 *
 *******************************************************************************
 *
 * @return  The new rank of u2 v2^T or -1 if ranks are too large for
 *          recompression
 *
 *******************************************************************************/
rpk_fixdbl_t
rpkx_zrradd_qr( const rpk_ctx_t    *ctx,
                rpk_zrrqr_cp_t      rrqrfct,
                rpk_trans_t         transAu,
                rpk_trans_t         transAv,
                const void         *alphaptr,
                rpk_int_t           M1,
                rpk_int_t           N1,
                const rpk_matrix_t *A,
                rpk_int_t           M2,
                rpk_int_t           N2,
                rpk_matrix_t       *B,
                rpk_int_t           offx,
                rpk_int_t           offy )
{
    rpk_int_t        rankA, rank, M, N, minV;
    rpk_int_t        i, ret, new_rank, rklimit;
    rpk_int_t        ldau, ldav, ldbu, ldbv, ldu, ldv;
    rpk_complex64_t *u1u2, *v1v2, *u;
    rpk_complex64_t *zbuf, *tauV;
    size_t           wzsize;
    double           tol = ctx->tolerance;

    assert( transAu == RapackNoTrans );

    /* PQRCP parameters / workspace */
    rpk_int_t        nb = 32;
    rpk_int_t        lwork;
    rpk_int_t       *jpvt;
    rpk_complex64_t *zwork, zzsize;
    double          *rwork;
    rpk_complex64_t  alpha = *( (rpk_complex64_t *)alphaptr );
    rpk_fixdbl_t     flops, total_flops = 0.;

#if defined(RAPACK_DEBUG_LR)
    if ( B->rk > 0 ) {
        int rc = rpk_zlrdbg_check_orthogonality( M2, B->rk, B->u, M2 );
        if ( rc == 1 ) {
            fprintf( stderr, "Failed to have B->u orthogonal in entry of rradd\n" );
        }
    }
#endif

    rankA = ( A->rk == -1 ) ? rpk_imin( M1, N1 ) : A->rk;
    rank  = rankA + B->rk;
    M     = rpk_imax( M2, M1 );
    N     = rpk_imax( N2, N1 );

    minV = rpk_imin( N, rank );

    assert( M2 == M && N2 == N );
    assert( B->rk != -1 );

    assert( A->rk <= A->rkmax );
    assert( B->rk <= B->rkmax );

    if ( ( ( M1 + offx ) > M2 ) ||
         ( ( N1 + offy ) > N2 ) )
    {
        rapack_print_error( "Dimensions are not correct" );
        assert( 0 /* Incorrect dimensions */ );
        return total_flops;
    }

    /*
     * A is rank null, nothing to do
     */
    if ( A->rk == 0 ) {
        return total_flops;
    }

    /*
     * Let's handle case where B is a null matrix
     *   B = alpha A
     */
    if ( B->rk == 0 ) {
        rpkx_zlrcpy( ctx, transAv, alpha,
                     M1, N1, A, M2, N2, B,
                     offx, offy );
        return total_flops;
    }

    /*
     * The rank is too big, let's try to compress
     */
    if ( rank > rpk_imin( M, N ) ) {
        assert( 0 );
    }

    /*
     * Let's define leading dimensions
     */
    ldau = ( A->rk == -1 ) ? A->rkmax : M1;
    ldav = ( transAv == RapackNoTrans ) ? A->rkmax : N1;
    ldbu = M;
    ldbv = B->rkmax;
    ldu  = M;
    ldv  = rank;

    /*
     * Let's compute the size of the workspace
     */
    /* u1u2 and v1v2 */
    wzsize = ( M + N ) * rank;
    /* tauV */
    wzsize += minV;

    /* RRQR workspaces */
    rklimit = rpk_imin( rank, ctx->get_rklimit( ctx, M, N ) );
    rrqrfct( tol, rklimit, 1, nb,
             rank, N, NULL, ldv,
             NULL, NULL,
             &zzsize, -1, NULL );
    lwork = (rpk_int_t)(zzsize);
    wzsize += lwork;

#if defined(RAPACK_DEBUG_LR)
    (void)wzsize;
    zbuf  = NULL;
    u1u2  = malloc( ldu * rank * sizeof(rpk_complex64_t) );
    v1v2  = malloc( ldv * N    * sizeof(rpk_complex64_t) );
    tauV  = malloc( rank       * sizeof(rpk_complex64_t) );
    zwork = malloc( lwork      * sizeof(rpk_complex64_t) );

    rwork = malloc( 2 * rpk_imax( rank, N ) * sizeof(double) );
#else
    zbuf = malloc( wzsize * sizeof(rpk_complex64_t)
                 + 2 * rpk_imax(rank, N) * sizeof(double) );

    u1u2  = zbuf;
    v1v2  = u1u2 + ldu * rank;
    tauV  = v1v2 + ldv * N;
    zwork = tauV + rank;

    rwork = (double *)( zwork + lwork );
#endif

    /*
     * Concatenate U2 and U1 in u1u2
     *  [ u2  0  ]
     *  [ u2  u1 ]
     *  [ u2  0  ]
     */
    rpk_zlrconcatenate_u( alpha,
                          M1, N1, A,
                          M2,     B,
                          offx, u1u2 );

    /*
     * Concatenate V2 and V1 in v1v2
     *  [ v2^h v2^h v2^h ]
     *  [ 0    v1^h 0    ]
     */
    rpk_zlrconcatenate_v( transAv, alpha,
                          M1, N1, A,
                              N2, B,
                          offy, v1v2 );

    /*
     * Orthogonalize [u2, u1]
     * [u2, u1] = [u2, u1 - u2(u2Tu1)] * (I u2Tu1)
     *                                   (0   I  )
     */

    /* We do not care is A was integrated into v1v2 */
    if ( rankA != 0 ) {
        switch ( ctx->orthmeth ) {
            case RapackCompressOrthoQR:
                flops = rpk_zlrorthu_fullqr( M, N, B->rk + rankA,
                                             u1u2, ldu, v1v2, ldv );
                break;

            case RapackCompressOrthoPartialQR:
                flops = rpk_zlrorthu_partialqr( M, N, B->rk, &rankA, offx, offy,
                                                u1u2, ldu, v1v2, ldv );
                break;

            case RapackCompressOrthoCGS:
                rapack_attr_fallthrough;

            default:
                flops = rpk_zlrorthu_cgs( M2, N2, M1, N1, B->rk, &rankA, offx, offy,
                                          u1u2, ldu, v1v2, ldv );
        }

        total_flops += flops;
    }

    rank = B->rk + rankA;

    if ( rankA == 0 ) {
        /*
         * The final B += A fit in B
         * Lets copy and return
         */
        ret = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'A', M, B->rk, u1u2, ldu, B->u, ldbu );
        assert( ret == 0 );
        ret = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'A', B->rk, N, v1v2, ldv, B->v, ldbv );
        assert( ret == 0 );

        free( zbuf );
#if defined(RAPACK_DEBUG_LR)
        free( u1u2 );
        free( v1v2 );
        free( tauV );
        free( zwork );
        free( rwork );
#endif
        return total_flops;
    }

    jpvt = malloc( rpk_imax( rank, N ) * sizeof( rpk_int_t ) );

    if ( ctx->use_reltol ) {
        /**
         * In relative tolerance, we can choose two solutions:
         *  1) The first one, more conservative, is to compress relatively to
         *  the norm of the final matrix \f$ \alpha A + B \f$. In this kernel, we
         *  exploit the fact that the V part contains all the information while
         *  the U part is orthonormal, and compute it as follow:
         *
         * double norm = LAPACKE_zlange_work( LAPACK_COL_MAJOR, 'f', rank, N,
         *                                    v1v2, ldv, NULL );
         * tol = tol * norm;
         *
         *  2) The second solution, less conservative, will allow to reduce the
         *  rank more efficiently. Since A and B have been compressed relatively
         *  to their respective norms, there is no reason to compress the sum
         *  relatively to its own norm, but it is more reasonable to compress it
         *  relatively to the norm of A and B. For example, A-B would be full
         *  with the first criterion, and rank null with the second.
         *  Note that here, we can only have an estimation that once again
         *  reduces the conservation of the criterion.
         *  \f[ || \alpha A + B || <= |\alpha| ||A|| + ||B|| <= |\alpha| ||U_aV_a|| + ||U_bV_b|| \f]
         *
         */
        double normA, normB;
        normA = rpk_zlrnrm( RapackFrobeniusNorm, transAv,       M1, N1, A );
        normB = rpk_zlrnrm( RapackFrobeniusNorm, RapackNoTrans, M2, N2, B );
        tol   = tol * ( cabs( alpha ) * normA + normB );
    }

    /*
     * Perform RRQR factorization on (I u2Tu1) v1v2 = (Q2 R2)
     *                               (0   I  )
     */
    rklimit  = rpk_imin( rklimit, rank );
    new_rank = rrqrfct( tol, rklimit, 1, nb,
                        rank, N, v1v2, ldv,
                        jpvt, tauV,
                        zwork, lwork, rwork );
    flops = (new_rank == -1) ? flops_zgeqrf( rank, N )
        :                     (flops_zgeqrf( rank, new_rank ) +
                               flops_zunmqr( rank, N-new_rank, new_rank, RapackLeft ));
    total_flops += flops;

    /*
     * First case: The rank is too big, so we decide to uncompress the result
     */
    if ( ( new_rank > rklimit ) || ( new_rank == -1 ) ) {
        rpk_matrix_t Bbackup = *B;

        rpk_zlralloc( M, N, -1, B );
        u = B->u;

        /* Uncompress B */
        flops = flops_zgemm( M, N, Bbackup.rk );
        cblas_zgemm( CblasColMajor, CblasNoTrans, CblasNoTrans,
                     M, N, Bbackup.rk,
                     CBLAS_SADDR(zone),  Bbackup.u, ldbu,
                                         Bbackup.v, ldbv,
                     CBLAS_SADDR(zzero), u,         M );
        total_flops += flops;

        /* Add A into it */
        if ( A->rk == -1 ) {
            flops = 2 * M1 * N1;
            rpk_zgeadd( transAv, M1, N1,
                        alpha, A->u, ldau,
                        zone, u + offy * M + offx, M);
        }
        else {
            flops = flops_zgemm( M1, N1, A->rk );
            cblas_zgemm( CblasColMajor, CblasNoTrans, (CBLAS_TRANSPOSE)transAv,
                         M1, N1, A->rk,
                         CBLAS_SADDR(alpha), A->u, ldau,
                         A->v, ldav,
                         CBLAS_SADDR(zone), u + offy * M + offx, M );
        }
        total_flops += flops;
        rpk_zlrfree( &Bbackup );
        free( zbuf );
        free( jpvt );
#if defined(RAPACK_DEBUG_LR)
        free( u1u2 );
        free( v1v2 );
        free( tauV );
        free( zwork );
        free( rwork );
#endif
        return total_flops;
    }
    else if ( new_rank == 0 ) {
        rpk_zlrfree( B );
        free( zbuf );
        free( jpvt );
#if defined(RAPACK_DEBUG_LR)
        free( u1u2 );
        free( v1v2 );
        free( tauV );
        free( zwork );
        free( rwork );
#endif
        return total_flops;
    }

    /*
     * We need to reallocate the buffer to store the new compressed version of B
     * because it wasn't big enough
     */
    ret = rpkx_zlrsze( ctx, 0, M, N, B, new_rank, -1, -1 );
    assert( ret != -1 );
    assert( B->rkmax >= new_rank );
    assert( B->rkmax >= B->rk );

    ldbv = B->rkmax;

    /* B->v = P v1v2 */
    {
        rpk_complex64_t *tmpV;
        rpk_int_t        lm;

        memset( B->v, 0, N * ldbv * sizeof( rpk_complex64_t ) );
        tmpV = B->v;
        for ( i = 0; i < N; i++ ) {
            lm = rpk_imin( new_rank, i + 1 );
            memcpy( tmpV + jpvt[i] * ldbv,
                    v1v2 + i       * ldv,
                    lm * sizeof(rpk_complex64_t) );
        }
    }

    /* Compute Q2 factor */
    {
        flops = flops_zungqr( rank, new_rank, new_rank )
            +   flops_zgemm( M, new_rank, rank );

        ret = LAPACKE_zungqr_work( LAPACK_COL_MAJOR, rank, new_rank, new_rank,
                                   v1v2, ldv, tauV, zwork, lwork );
        assert( ret == 0 );

        cblas_zgemm(CblasColMajor, CblasNoTrans, CblasNoTrans,
                     M, new_rank, rank,
                     CBLAS_SADDR(zone),  u1u2, ldu,
                                         v1v2, ldv,
                     CBLAS_SADDR(zzero), B->u, ldbu);
        total_flops += flops;
    }

    free( zbuf );
    free( jpvt );

#if defined(RAPACK_DEBUG_LR)
    free( u1u2 );
    free( v1v2 );
    free( tauV );
    free( zwork );
    free( rwork );
#endif

    (void)ret;
    return total_flops;
}
