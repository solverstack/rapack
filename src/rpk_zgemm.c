/**
 *
 * @file rpk_zgemm.c
 *
 * RAPACK low-rank kernel routines to compute a matrix matrix product in either
 * form low or full rank.
 *
 * @copyright 2016-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Gregoire Pichon
 * @author Pierre Ramet
 * @author Abel Calluaud
 * @date 2024-03-11
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

/**
 *******************************************************************************
 *
 * @brief Compute the matrix matrix product when applied to a full rank matrix.
 *
 * This function considers that the C matrix is full rank, and A and B are
 * either full-rank or low-rank.  The result of the product is directly applied
 * to the C matrix.
 *
 *******************************************************************************
 *
 * @param[in] ctx
 *          The rapack context that defines the compression parameters to use.
 *
 * @param[inout] params
 *          The LRMM structure that stores all the parameters used in the LRMM
 *          functions family.
 *          @sa rpk_zgemm_t
 *
 *******************************************************************************
 *
 * @return The number of flops required to perform the operation.
 *
 *******************************************************************************/
static inline rpk_fixdbl_t
rpkx_zgemm_Cfr( const rpk_ctx_t *ctx, rpk_zgemm_t *params )
{
    (void)ctx;
    const rpk_matrix_t *A     = params->A;
    const rpk_matrix_t *B     = params->B;
    rpk_fixdbl_t        flops = 0.0;

    assert( A->rk <= A->rkmax && A->rk != 0 );
    assert( B->rk <= B->rkmax && B->rk != 0 );
    assert( params->C->rk == -1 );

    if ( A->rk == -1 ) {
        if ( B->rk == -1 ) {
            flops = rpkx_zfrfr2fr( ctx, params );
        }
        else {
            flops = rpkx_zfrlr2fr( ctx, params );
        }
    }
    else {
        if ( B->rk == -1 ) {
            flops = rpkx_zlrfr2fr( ctx, params );
        }
        else {
            flops = rpkx_zlrlr2fr( ctx, params );
        }
    }

    assert( params->C->rk == -1 );

    return flops;
}

/**
 *******************************************************************************
 *
 * @brief Compute the matrix matrix product when applied to a null matrix.
 *
 * This function considers that the C matrix is null, and A and B are either
 * full-rank or low-rank.  The result of the product is directly applied to the
 * C matrix.
 *
 *******************************************************************************
 *
 * @param[in] ctx
 *          The rapack context that defines the compression parameters to use.
 *
 * @param[inout] params
 *          The LRMM structure that stores all the parameters used in the LRMM
 *          functions family.
 *          @sa rpk_zgemm_t
 *
 *******************************************************************************
 *
 * @return The number of flops required to perform the operation.
 *
 *******************************************************************************/
static inline rpk_fixdbl_t
rpkx_zgemm_Cnull( const rpk_ctx_t *ctx, rpk_zgemm_t *params )
{
    PASTE_RPK_ZLRMM_PARAMS( params );
    rpk_matrix_t AB;
    int          infomask = 0;
    rpk_fixdbl_t flops    = 0.0;

    assert( A->rk <= A->rkmax && A->rk != 0 );
    assert( B->rk <= B->rkmax && B->rk != 0 );

    if ( A->rk == -1 ) {
        if ( B->rk == -1 ) {
            flops = rpkx_zfrfr2lr( ctx, params, &AB, &infomask,
                                   rpk_imin( rpk_imin( M, N ),
                                             ctx->get_rklimit( ctx, Cm, Cn ) ) );
        }
        else {
            flops = rpkx_zfrlr2lr( ctx, params, &AB, &infomask,
                                   rpk_imin( M, ctx->get_rklimit( ctx, Cm, Cn ) ) );
        }
    }
    else {
        if ( B->rk == -1 ) {
            flops = rpkx_zlrfr2lr( ctx, params, &AB, &infomask,
                                   rpk_imin( N, ctx->get_rklimit( ctx, Cm, Cn ) ) );
        }
        else {
            flops = rpkx_zlrlr2lr( ctx, params, &AB, &infomask );

            assert( AB.rk != -1 );
            assert( AB.rkmax != -1 );
        }
    }

    flops += rpkx_zlradd( ctx, params, &AB, infomask );

    /* Free memory from zlrm3 */
    if ( infomask & RAPACK_LRM3_ALLOCU ) {
        free( AB.u );
    }
    if ( infomask & RAPACK_LRM3_ALLOCV ) {
        free( AB.v );
    }

    PASTE_RPK_ZLRMM_VOID;

    return flops;
}

/**
 *******************************************************************************
 *
 * @brief Compute the matrix matrix product when applied to a low rank matrix.
 *
 * This function considers that the C matrix is low rank, and A and B are
 * either full-rank or low-rank.  The result of the product is directly applied
 * to the C matrix.
 *
 *******************************************************************************
 *
 * @param[in] ctx
 *          The rapack context that defines the compression parameters to use.
 *
 * @param[inout] params
 *          The LRMM structure that stores all the parameters used in the LRMM
 *          functions family.
 *          @sa rpk_zgemm_t
 *
 *******************************************************************************
 *
 * @return The number of flops required to perform the operation.
 *
 *******************************************************************************/
static inline rpk_fixdbl_t
rpkx_zgemm_Clr( const rpk_ctx_t *ctx, rpk_zgemm_t *params )
{
    PASTE_RPK_ZLRMM_PARAMS( params );
    rpk_matrix_t AB;
    int          infomask = 0;
    rpk_fixdbl_t flops    = 0.0;

    assert( A->rk <= A->rkmax && A->rk != 0 );
    assert( B->rk <= B->rkmax && B->rk != 0 );

    if ( A->rk == -1 ) {
        if ( B->rk == -1 ) {
            flops = rpkx_zfrfr2lr( ctx, params, &AB, &infomask, rpk_imin( M, N ) );
        }
        else {
            flops = rpkx_zfrlr2lr( ctx, params, &AB, &infomask, M );
        }
    }
    else {
        if ( B->rk == -1 ) {
            flops = rpkx_zlrfr2lr( ctx, params, &AB, &infomask, N );
        }
        else {
            flops = rpkx_zlrlr2lr( ctx, params, &AB, &infomask );

            assert( AB.rk != -1 );
            assert( AB.rkmax != -1 );
        }
    }

    flops += rpkx_zlradd( ctx, params, &AB, infomask );

    /* Free memory from zlrm3 */
    if ( infomask & RAPACK_LRM3_ALLOCU ) {
        free( AB.u );
    }
    if ( infomask & RAPACK_LRM3_ALLOCV ) {
        free( AB.v );
    }

    PASTE_RPK_ZLRMM_VOID;

    return flops;
}

/**
 *******************************************************************************
 *
 * @brief Compute the matrix matrix product when involved matrices are stored in
 * a low-rank structure.
 *
 * This function considers the generic matrix matrix product added to a third
 * matric C. All matrices are either null, low-rank or full-rank.
 *
 *******************************************************************************
 *
 * @param[inout] params
 *          The LRMM structure that stores all the parameters used in the LRMM
 *          functions family.
 *          @sa rpk_zgemm_t
 *
 *******************************************************************************
 *
 * @return The number of flops required to perform the operation.
 *
 *******************************************************************************/
rpk_fixdbl_t
rpkx_zgemm( const rpk_ctx_t *ctx, rpk_zgemm_t *params )
{
    PASTE_RPK_ZLRMM_PARAMS( params );
    rpk_fixdbl_t flops = 0.0;

    assert( A->rk <= A->rkmax );
    assert( B->rk <= B->rkmax );
    assert( C->rk <= C->rkmax );

    /* Quick return if alpha * A * B is 0 */
    if ( ( A->rk == 0 ) || ( B->rk == 0 ) || ( alpha == 0.0 ) ) {
        flops += rpkx_zlrscl( ctx, beta, M, N, Cm, Cn, C, offx, offy );
        return flops;
    }

    params->lwused = 0;

    /* TODO: this is a temporary fix */
    if ( lwork == 0 ) {
        params->work = work = NULL;
    }
    assert( ( ( work != NULL ) && ( lwork >  0 ) ) ||
            ( ( work == NULL ) && ( lwork <= 0 ) ) );

    if ( C->rk == 0 ) {
        flops = rpkx_zgemm_Cnull( ctx, params );
    }
    else if ( C->rk == -1 ) {
        flops = rpkx_zgemm_Cfr( ctx, params );
    }
    else {
        flops = rpkx_zgemm_Clr( ctx, params );
    }

#if defined( RAPACK_DEBUG_LR )
    ctx->lock( ctx, lock );
    if ( ( C->rk > 0 ) && ( ctx->rpk_ge2lr != rpkx_zge2lr_svd ) ) {
        int rc = rpk_zlrdbg_check_orthogonality( Cm, C->rk, (rpk_complex64_t *)C->u, Cm );
        if ( rc == 1 ) {
            fprintf( stderr, "Failed to have u orthogonal in exit of gemm\n" );
        }
    }
    ctx->unlock( ctx, lock );
#endif

    PASTE_RPK_ZLRMM_VOID;
    return flops;
}
