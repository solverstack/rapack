#
#  @file rapack.pc
#
#  @copyright 2016-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
#                       Univ. Bordeaux. All rights reserved.
#
#  @version 1.0.0
#  @author Mathieu Faverge
#  @author Abel Calluaud
#  @date 2023-12-14
#
prefix=@CMAKE_INSTALL_PREFIX@
exec_prefix=${prefix}
libdir=${exec_prefix}/lib
includedir=${exec_prefix}/include

Name: RAPACK
Description: Low Rank Algebra Package
Version: @RAPACK_VERSION_MAJOR@.@RAPACK_VERSION_MINOR@.@RAPACK_VERSION_MICRO@
Cflags: -I${includedir} @RAPACK_PKGCONFIG_CFLAGS@
Libs: -L${libdir} -lrapack @RAPACK_PKGCONFIG_LIBS@
Libs.private: @RAPACK_PKGCONFIG_LIBS_PRIVATE@
Requires: @RAPACK_PKGCONFIG_REQUIRED@
Requires.private: @RAPACK_PKGCONFIG_REQUIRED_PRIVATE@
