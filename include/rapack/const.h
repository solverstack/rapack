/**
 *
 * @file rapack/const.h
 *
 * Rapack API enums parameters.
 *
 * @copyright 2004-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Pierre Ramet
 * @author Mathieu Faverge
 * @author Tony Delarue
 * @author Abel Calluaud
 * @date 2024-02-14
 *
 * @addtogroup rapack_const
 * @{
 *
 **/
#ifndef _rapack_const_h_
#define _rapack_const_h_

#include "rapack/config.h"

BEGIN_C_DECLS

/**
 * @brief Error codes
 */
typedef enum rapack_error_e {
    RAPACK_SUCCESS            = 0,  /**< No error                     */
    RAPACK_ERR_UNKNOWN        = 1,  /**< Unknown error                */
    RAPACK_ERR_ALLOC          = 2,  /**< Allocation error             */
    RAPACK_ERR_NOTIMPLEMENTED = 3,  /**< Not implemented feature      */
    RAPACK_ERR_OUTOFMEMORY    = 4,  /**< Not enough memory            */
    RAPACK_ERR_THREAD         = 5,  /**< Error with threads           */
    RAPACK_ERR_INTERNAL       = 6,  /**< Internal error               */
    RAPACK_ERR_BADPARAMETER   = 7,  /**< Bad parameters given         */
    RAPACK_ERR_FILE           = 8,  /**< Error in In/Out operations   */
    RAPACK_ERR_INTEGER_TYPE   = 9,  /**< Error with integer types     */
    RAPACK_ERR_IO             = 10, /**< Error with input/output      */
    RAPACK_ERR_MPI            = 11  /**< Error with MPI calls         */
} rapack_error_t;

/**
 *
 * @name Constants compatible with CBLAS & LAPACK & PLASMA
 * @{
 *    The naming and numbering of the following constants is consistent with:
 *
 *       - CBLAS from Netlib (http://www.netlib.org/blas/blast-forum/cblas.tgz)
 *       - C Interface to LAPACK from Netlib (http://www.netlib.org/lapack/lapwrapc/)
 *       - Plasma (http://icl.cs.utk.edu/plasma/index.html)
 *
 */

/**
 * @brief Arithmetic types.
 *
 * This describes the different arithmetics that can be stored in a sparse matrix.
 * @remark The values start at 2 for compatibility purpose with PLASMA and
 * DPLASMA libraries.
 */
typedef enum rpk_coeftype_e {
    RapackFloat     = 2, /**< Single precision real              */
    RapackDouble    = 3, /**< Double precision real              */
    RapackComplex32 = 4, /**< Single precision complex           */
    RapackComplex64 = 5  /**< Double precision complex           */
} rpk_coeftype_t;

/**
 * @brief Direction of the matrix storage
 */
typedef enum rpk_layout_e {
    RapackRowMajor  = 101, /**< Storage in row major order    */
    RapackColMajor  = 102  /**< Storage in column major order */
} rpk_layout_t;

/**
 * @brief Transposition enum.
 */
typedef enum rpk_trans_e {
    RapackNoTrans   = 111, /**< Use A         */
    RapackTrans     = 112, /**< Use A^t       */
    RapackConjTrans = 113  /**< Use conj(A^t) */
} rpk_trans_t;

/**
 * @brief Function to check the transposition value independently from the file arithmetic precision.
 */
static inline int
rpk_is_valid_trans( rpk_trans_t trans )
{
    if ( (trans >= RapackNoTrans) && (trans <= RapackConjTrans) ) {
        return 1;
    }
    else {
        return 0;
    }
}

/**
 * @brief Upper/Lower part
 */
typedef enum rpk_uplo_e {
    RapackUpper      = 121, /**< Upper part of the matrix */
    RapackLower      = 122, /**< Lower part of the matrix */
    RapackUpperLower = 123  /**< Upper and lower part of the matrix */
} rpk_uplo_t;

/**
 * @brief Diagonal
 */
typedef enum rpk_diag_e {
    RapackNonUnit = 131, /**< Diagonal is non unitary */
    RapackUnit    = 132  /**< Diagonal is unitary     */
} rpk_diag_t;

/**
 * @brief Side of the operation
 */
typedef enum rpk_side_e {
    RapackLeft  = 141, /**< Apply operator on the left  */
    RapackRight = 142  /**< Apply operator on the right */
} rpk_side_t;

/**
 * @brief Norms
 */
typedef enum rpk_normtype_e {
    RapackOneNorm       = 171, /**< One norm:       max_j( sum_i( |a_{ij}| ) )   */
    RapackFrobeniusNorm = 174, /**< Frobenius norm: sqrt( sum_{i,j} (a_{ij}^2) ) */
    RapackInfNorm       = 175, /**< Inifinite norm: max_i( sum_j( |a_{ij}| ) )   */
    RapackMaxNorm       = 177  /**< Max norm:       max_{i,j}( | a_{ij} | )      */
} rpk_normtype_t;

/**
 * @brief Compression methods available
 */
typedef enum rpk_compmeth_e {
    RapackCompressMethodSVD,   /**< Use singular value decomposition for low-rank compression       */
    RapackCompressMethodPQRCP, /**< Use partial QR with column pivoting for low-rank compression    */
    RapackCompressMethodRQRCP, /**< Use randomized QR with column pivoting for low-rank compression */
    RapackCompressMethodTQRCP, /**< Use truncated QR with column pivotingfor low-rank compression   */
    RapackCompressMethodRQRRT, /**< Use randomized QR with rotation for low-rank compression        */
    RapackCompressMethodNbr    /**< Total number of available compression methods                   */
} rpk_compmeth_t;

/**
 * @brief Orthogonalization methods available
 */
typedef enum rpk_orthmeth_e {
    RapackCompressOrthoCGS,       /**< Orthogonalize low-rank bases with Gram-Schimdt                                           */
    RapackCompressOrthoQR,        /**< Orthogonalize low-rank bases with QR decomposition                                       */
    RapackCompressOrthoPartialQR  /**< Orthogonalize low-rank bases with projections in orthogonal space followed by smaller QR */
} rpk_orthmeth_t;

/**
 * @}
 */

extern char *rpk_lapack_consts[];
#define rpk_lapack_const(rpk_const) rpk_lapack_consts[rpk_const][0]

END_C_DECLS

#endif /* _rapack_const_h_ */

/**
 * @}
 */
