/**
 *
 * @file testrpk_zge2lr_tests.c
 *
 * Tests and validate the Xge2lr routine.
 *
 * @copyright 2015-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Gregoire Pichon
 * @author Esragul Korkmaz
 * @author Mathieu Faverge
 * @date 2024-01-08
 *
 * @precisions normal z -> z c d s
 *
 **/
#include "tests.h"
#include "testrpk_z.h"

int main( int argc, char **argv )
{
    test_matrix_t A;
    rpk_int_t n;
    int mode, p, i, ret, rc = 0;
    test_param_t params;
    double eps = LAPACKE_dlamch_work('e');
    rpk_ctx_t lowrank;

    testGetOptions( argc, argv, &params, eps );

    fprintf( stdout, "%7s %4s %12s %12s %12s %12s\n",
             "Method", "Rank", "Time", "||A||_f", "||A-UVt||_f",
             "||A-UVt||_f/(||A||_f * eps)" );

    lowrank.use_reltol = params.use_reltol;
    lowrank.tolerance  = params.tol_cmp;
    lowrank.rpk_ge2lr = rpkx_zge2lr_svd;
    lowrank.rpk_rradd = rpkx_zrradd_svd;

    for (n=params.n[0]; n<=params.n[1]; n+=params.n[2]) {
        A.m  = n;
        A.n  = n;
        A.ld = n;
        A.fr = malloc( A.ld * A.n * sizeof(rpk_complex64_t) );

        for (p=params.prank[0]; p<=params.prank[1]; p+=params.prank[2]) {
            A.rk = (p * n) / 100;

            for (mode=params.mode[0]; mode<=params.mode[1]; mode+=params.mode[2])
            {
                printf( "   -- Test GE2LR TolGen=%e TolCmp=%e M=N=LDA=%ld R=%ld MODE=%d\n",
                        params.tol_gen, lowrank.tolerance, (long)A.n, (long)A.rk, mode );

                /*
                 * Generate a matrix of a given rank for the prescribed tolerance
                 */
                testrpk_zgenmat( mode, params.tol_gen,
				 params.threshold, &A );
		
                /* Let's test all methods we have */
                for(i=params.method[0]; i<=params.method[1]; i+=params.method[2])
                {
                    lowrank.rpk_ge2lr = rpk_ge2lr_functions[i][RapackComplex64-2];
                    lowrank.rpk_rradd = rpk_rradd_functions[i][RapackComplex64-2];

                    ret = testrpk_zcheck_ge2lr( &lowrank, i, &A );
                    rc += (ret ? 1 : 0 );
                }
            }
        }
        free(A.fr);
    }

    if( rc == 0 ) {
        printf( " -- All tests PASSED --\n" );
        return EXIT_SUCCESS;
    }
    else
    {
        printf( " -- %d tests FAILED --\n", rc );
        return EXIT_FAILURE;
    }
}
