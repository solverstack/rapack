/**
 *
 * @file testrpk_zge2lr_performance_realcase.c
 *
 * Tests and validate the Xge2lr routine.
 *
 * @copyright 2015-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Esragul Korkmaz
 * @date 2023-12-19
 *
 * @precisions normal z -> z c d s
 *
 **/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1
#endif
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <pastix.h>
#include "common.h"
#include <lapacke.h>
#include <cblas.h>
#include "blend/solver.h"
#include "kernels/rpk_zcores.h"
#include "kernels/rpk_zlrcores.h"
#include "kernels/rpk_lowrank.h"
#include "flops.h"
#include "testrpk_z.h"
#include "tests.h"

double
testrpk_zge2lr_performance( FILE *f, rpk_compress_method_t method, double tol_cmp,
                             int m, int n, rpk_complex64_t *A, rpk_int_t lda,
                             double normA, char *fn )
{
    fct_ge2lr_t rpk_ge2lr = rpk_ge2lr_functions[method][RapackComplex64-2];
    rpk_complex64_t *A2;
    rpk_matrix_t    lrA;
    rpk_int_t minMN = rpk_imin(m, n);
    rpk_fixdbl_t flops, gflops;
    double resid, normR, timer;
    int crank;

    if (m < 0) {
        fprintf(stderr, "Invalid m parameter\n");
        return -4;
    }
    if (n < 0) {
        fprintf(stderr, "Invalid n parameter\n");
        return -5;
    }
    if (lda < m) {
        fprintf(stderr, "Invalid lda parameter\n");
        return -6;
    }

    /* Backup A in A2 */
    A2 = malloc( m * n * sizeof(rpk_complex64_t));
    LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'A', m, n,
                         A, lda, A2, m );

    /* Compress A */
    clockStart(timer);
    rpk_ge2lr( 1, tol_cmp, minMN, m, n, A, lda, &lrA );
    clockStop(timer);
    timer = clockVal(timer);

    crank = lrA.rk;

    flops = flops_zgeqrf( m, crank ) +
        flops_zunmqr( m, n - crank, crank, RapackLeft ) +
        flops_zungqr( m, crank, crank);
    gflops = flops * 1e-9 / timer;

    /*
     * Let's check the result
     */
    rpk_zlr2ge( RapackNoTrans, m, n,
                 &lrA, A2, lda );

    rpk_zgeadd( RapackNoTrans, m, n,
                 -1., A,  lda,
                  1., A2, lda );

    /* Frobenius norm of ||A - (U_i *V_i)|| */
    normR = LAPACKE_zlange_work( LAPACK_COL_MAJOR, 'f', m, n, A2, lda, NULL );
    resid = normR / ( tol_cmp * normA );

    if ( f == stdout ) {
	fprintf( stdout, "%s;%d;%e;%e;%d;%e;%e;%e;%e;%s;%s\n",
                 rpk_compmeth_shnames[method],
                 n, normA,
                 tol_cmp, crank, timer, gflops, normR, resid,
                 (resid > 10.) ? "FAILED" : "SUCCESS", fn );

   }
    else {
        fprintf( stdout, "%s;%d;%e;%e;%d;%e;%e;%e;%e;%s;%s\n",
                 rpk_compmeth_shnames[method],
                 n, normA,
                 tol_cmp, crank, timer, gflops, normR, resid,
                 (resid > 10.) ? "FAILED" : "SUCCESS", fn );
    }
    free(A2);
    rpk_zlrfree(&lrA);
    return (resid > 10.);
}

/*
 * Performance test code for the real case matrices. Matrices can be downloaded from https://sparse.tamu.edu/
 * These sparse matrices are converted into dense matrix for seeing the performance of the compression kernels on these matrices 
 * */
int main( int argc, char **argv )
{
	//spm_complex64_t *A;
	rpk_complex64_t *A;
	rpk_int_t    iparm[IPARM_SIZE];  /*< Integer in/out parameters for pastix                */
    	double          dparm[DPARM_SIZE];  /*< Floating in/out parameters for pastix               */
	rpk_int_t n, m, lda;
    	double normA, normspm;
    	char           *filename, *fname, *fnm;
	int             rc=0, i, check = 1, ret;
    	spm_driver_t    driver;
	spmatrix_t     *spm;
    	//double eps = LAPACKE_dlamch_work('e');
	double tol_cmp = 1e-8; //sqrt(eps);
	FILE * f2;
        /**
 	* Initialize parameters to default values
 	*/
    	pastixInitParam( iparm, dparm );

    	/**
       	* Get options from command line
 	*/
    	pastixGetOptions( argc, argv,
                          iparm, dparm,
                          &check, &driver, &filename );
	/**
 	* Read the sparse matrix with the driver
        */
    	spm = malloc( sizeof( spmatrix_t ) );
    	spmReadDriver( driver, filename, spm );

	/* get the m and n information (for rectangular case! n exists but not m)*/
	f2 = fopen(filename, "r");
	char mystring [1000], c;
	while(fgets( mystring, 1000, f2) != NULL) {
		if (mystring[0] == '%')
			continue;
		else {
			char *token = strtok(mystring, " "); 
			m = atoi(token);
        		token = strtok(NULL, " "); 
			n = atoi(token);
			break;
		}
	}
	fclose( f2 );
	
    	//spmPrintInfo( spm, stdout );
	normspm = spmNorm( 177, spm ); //max column norm
	spmScalMatrix(1./normspm, spm);
	normspm = spmNorm( 177, spm ); //max column norm

	A = spm2Dense( spm );
        normA = LAPACKE_zlange( LAPACK_COL_MAJOR, 'M', spm->gNexp, spm->gNexp, A, spm->gNexp ); //max column norm
        //n = (long)spm->gN;
        //m = n; 
        lda = m;


    	fprintf( stdout,  "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s\n",
                 "Method",
                 "N", "NormA",
                 "TolCmp", "CRank", "Time", "GFlops", "||A-UVt||_f", "||A-UVt||_f/(||A||_f*eps)", "Check", "MatName" );
	
        /* Let's test all methods we have */
        for(i=0; i<=4; i+=1) {
              	ret = testrpk_zge2lr_performance( i, tol_cmp,
                                                       m, n, A, lda, normA, filename );
        	rc += (ret ? 1 : 0 );
        }

       	free(A);
	free(filename);
    	if( rc == 0 ) {
        	printf( " -- All tests PASSED --\n" );
        	return EXIT_SUCCESS;
    	}
    	else
    	{
        	printf( " -- %d tests FAILED --\n", rc );
        	return EXIT_FAILURE;
    	}
}
