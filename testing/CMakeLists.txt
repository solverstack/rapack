###
#
#  @copyright 2013-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
#                       Univ. Bordeaux. All rights reserved.
#
#  @version 1.0.0
#  @author Mathieu Faverge
#  @author Esragul Korkmaz
#  @author Florent Pruvost
#  @author Pierre Ramet
#  @author Tony Delarue
#  @author Abel Calluaud
#  @date 2024-03-11
#
###
include(RulesPrecisions)

if(NOT TMG_FOUND)
  return()
endif()

### Generate the headers in all precisions
set(HEADERS
  testrpk_z.h
)

## reset variables
set(generated_headers "")

precisions_rules_py(generated_headers
  "${HEADERS}"
  PRECISIONS "s;d;c;z")

set(tests_headers
  ${generated_headers}
  tests.h
)

add_custom_target(tests_headers_tgt
  DEPENDS ${tests_headers} )

## Generate the test library files for all required precisions
set(LIB_SOURCES
  testrpk_zcommon.c
)

## reset variables
set(generated_libfiles "")

precisions_rules_py(generated_libfiles
  "${LIB_SOURCES}"
  PRECISIONS "s;d;c;z")

add_library( rpktests
  ${generated_libfiles}
  tests_auxiliary.c
)

add_dependencies( rpktests
  tests_headers_tgt
)

target_include_directories( rpktests PRIVATE
  $<BUILD_INTERFACE:${RAPACK_SOURCE_DIR}/src>
  $<BUILD_INTERFACE:${RAPACK_BINARY_DIR}/src> )

target_include_directories( rpktests PUBLIC
  $<BUILD_INTERFACE:${RAPACK_SOURCE_DIR}/testing>
  $<BUILD_INTERFACE:${RAPACK_BINARY_DIR}/testing> )

target_link_libraries( rpktests PUBLIC
  rapack
  MORSE::LAPACKE
  MORSE::TMG
  MORSE::CBLAS
  MORSE::M
)

set(TESTS_SOURCES
  testrpk_zge2lr_performance.c
  testrpk_zge2lr_stability.c
  testrpk_zge2lr_tests.c
  testrpk_zgemm_tests.c
  testrpk_zrradd_tests.c
)

## reset variables
set(generated_files)

precisions_rules_py(generated_files
  "${TESTS_SOURCES}"
  PRECISIONS "s;d;c;z")

set( testings
  ${generated_files} )

foreach (_file ${testings})
  get_filename_component(_name_we ${_file} NAME_WE)
  add_executable(${_name_we} ${_file})

  target_include_directories( ${_name_we} PRIVATE
    $<BUILD_INTERFACE:${RAPACK_SOURCE_DIR}/src>
    $<BUILD_INTERFACE:${RAPACK_BINARY_DIR}/src> )

  target_include_directories( ${_name_we} PUBLIC
    $<BUILD_INTERFACE:${RAPACK_SOURCE_DIR}/testing>
    $<BUILD_INTERFACE:${RAPACK_BINARY_DIR}/testing> )

  target_link_libraries( ${_name_we} PRIVATE
    rpktests
    rapack
    MORSE::LAPACKE
    MORSE::CBLAS
    MORSE::M
  )

  add_test( test_${_name_we} ./${_name_we} -n 300 -p 10 )
endforeach()
